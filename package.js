Package.describe({
  name: 'votercircle:meteor-lumx',
  version: '1.2.1',
  summary: 'LumX UI framework wrapped for Meteor',
  git: 'https://bitbucket.org/votercircle/meteor-lumx',
  documentation: 'README.md'
});

Package.onUse(function(api) {
    // meteor version
    api.versionsFrom('1.0');

    api.use('urigo:angular@0.8.0');
    api.use('wolves:bourbon@1.0.0');
    api.use('velocityjs:velocityjs@1.2.1');
    api.use('momentjs:moment@2.10.3');
    api.use('fourseven:scss@2.1.1');


    api.addAssets(['fonts/materialdesignicons-webfont.eot', 'fonts/materialdesignicons-webfont.svg', 'fonts/materialdesignicons-webfont.ttf',
        'fonts/materialdesignicons-webfont.woff', 'fonts/materialdesignicons-webfont.woff2', 'css/materialdesignicons.css.map'], 'client');
    api.addFiles(['css/materialdesignicons.css'], 'client');
    api.addFiles(['js/lumx.js', 'css/lumx.css'], 'client');
});